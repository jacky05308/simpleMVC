<?php


	define('APP_REAL_PATH', str_replace('\\', '/', __DIR__ ));
	require_once APP_REAL_PATH.'/../config/app.php';
	require_once APP_REAL_PATH.'/../core/core.php';

	$include_path[] = APP_REAL_PATH . '/../app/kernel';
	$include_path[] = APP_REAL_PATH . '/../app/kernel/database';
	$include_path[] = APP_REAL_PATH . '/../app/controller';
	$include_path[] = APP_REAL_PATH . '/../app/model';
	$include_path[] = APP_REAL_PATH . '/../app/view';

	// 設定新的 include_path
	set_include_path(join(PATH_SEPARATOR, $include_path));
	function __autoload($ClassName)
	{
	    require_once $ClassName . '.php';
	}


?>