<?php
function view($_ViewName,$data=[]){
	$_ViewName=str_replace('.', '/', $_ViewName);
	extract ($data);
	ob_start();
	   include ( APP_REAL_PATH . '/../template/'.$_ViewName.'.tpl.php' );
       $_ViewFile = ob_get_contents();
    ob_end_clean();
    return $_ViewFile;
}
function asset($name){
	echo  'http://'.$_SERVER['HTTP_HOST'].'/asset/'.$name;
}
function route($name,$data=[]){
	$RouteName=Route::$route_name[$name];
	foreach ($data as $key => $value) {
		$RouteName=str_replace($key, $value, $RouteName);
	}
	return 'http://'.$_SERVER['HTTP_HOST'].$RouteName;
}