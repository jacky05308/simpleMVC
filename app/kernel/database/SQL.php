<?php
class SQL extends DBConn
{
	static protected $dataTable=null;
	static protected $queryStr='';
	static protected $stmtArr=array();

	static function setDataTable($dataTable){
		self::getDB();
		self::$dataTable=$dataTable;
	}

	static function select($arrParams = null){
		$tmpStr=(is_array($arrParams)?implode(',',$arrParams):'*');

		self::$queryStr="SELECT $tmpStr FROM ".self::$dataTable;
		return new self;
	}
	static function insert($arrParams =null){
		$tmpStr=(is_array($arrParams)?'('.implode(',', $arrParams).')':'');

		self::$queryStr="INSERT INTO ".self::$dataTable.$tmpStr;
		return new self;
	}
	static function test(){
		return self::$queryStr;
	}
	static function update($arrParams){
		$tmpStr="UPDATE ".self::$dataTable." SET ";

		foreach ($arrParams[0] as $key => $value) {
			if($key!=0){
				$tmpStr.=',';
			}
			$tmpStr.="`$value` = :$value ";
			self::$stmtArr[":$value"]=$arrParams[1][$key];
		}


		self::$queryStr=$tmpStr;
		return new self;
	}

	static function delete(){
		$tmpStr="DELETE FROM ".self::$dataTable." ";
		self::$queryStr=$tmpStr;

		return new self;
	}


	static function value($arrParams){
		$tmpStr="VALUES ";
		foreach($arrParams as $key => $value){

			if($key!=0){
				$tmpStr.=',';
			}

			$tmpStr.='(';

			foreach ($value as $key1 => $value1) {

				if($key1!=0){
					$tmpStr.=',';
				}

				$params=':tmpArr_'.$key.'_'.$key1;
				$tmpStr.=$params;

				self::$stmtArr[$params]=$value1;
			}
			$tmpStr.=')';
		}

		self::$queryStr.=$tmpStr;

		return new self;
	}

	static function where($arrParams=null){
		$tmpStr=" WHERE ";
		//四個參數
		//第一個 參數1
		//第二個 方法
		//第三個 參數2
		if( $arrParams!=null ){

			foreach ($arrParams as $key => $value) {
				if($value[1]=='BETWEEN'){
					$params0=':'.$value[0].'_where_0';
					$params1=':'.$value[0].'_where_1';

					$tmpStr.="`$value[0]` $value[1] $params0 AND $params0 " ;
					self::$stmtArr[$params0]=$value[2][0];
					self::$stmtArr[$params1]=$value[2][1];
				}else{
					$params=':'.$value[0].'_where';

					$tmpStr.="`$value[0]` $value[1] $params ";
					self::$stmtArr[$params]=$value[2];
				}
				if(isset($value[3])){
					$tmpStr.="$value[3] ";
				}
			}
		}else{
			$tmpStr.=" 1";
		}

		self::$queryStr.=$tmpStr;

		return new self;

	}
	static function order($arrParams){
		$tmpStr = implode(',' , $arrParams);
		self::$queryStr.=" ORDER BY $tmpStr";

		return new self;
	}
	static function limit($par,$par1 = 0){
		self::$queryStr.=" LIMIT $par OFFSET $par1";
		return new self;
	}
	static function execute(){

		try{

	  		$stmt = self::$db->prepare(self::$queryStr);
	        $stmt->execute(self::$stmtArr);
	        $SQLResult['SQL_status']=true;
			$SQLResult['SQL_data']=$stmt->fetchAll(PDO::FETCH_ASSOC);

		}catch(Exception $e){

			$SQLResult['SQL_status']=false;

		}
		self::$queryStr='';
		self::$stmtArr=array();
		return $SQLResult;

	}
	static function Customsize($qStr,$sArr=array()){
		self::$queryStr=$qStr;
		self::$stmtArr=$sArr;
		return new self;
	}

}