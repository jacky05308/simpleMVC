<?php
 class Model{
	//protected $DataTable;
	protected $PrimaryKey='id';
	private $dataParams;
	function add(){
		/*insert {DataTable} values ({$array})*/
	}
	function find($param){
		/*select * from {DataTable} where {key} = {value} */
		SQL::setDataTable('test');
		$tmp=SQL::select()->where([[$this->PrimaryKey,'=',$param]])->execute();
		$this->dataParams=$tmp['SQL_data'];
		return $this;
	}
	function all(){
		SQL::setDataTable('test');
		$tmp=SQL::select()->execute();
		$this->dataParams=$tmp['SQL_data'];
		return $this;
	}
	function first(){
		if(isset($this->dataParams[0])){
			$this->dataParams=$this->dataParams[0];
		}else{
			$this->dataParams=null;
		}
		return $this;
	}
	function noFirst($param=1){
		if(isset($this->dataParams[$param-1])){
			$this->dataParams=$this->dataParams[$param-1];
		}else{
			$this->dataParams=null;
		}
		return $this;
	}
	function take($param){
		$cou=count($this->dataParams);
		if($cou<$param){
			$this->dataParams=array_slice($this->dataParams,0,$cou);
		}else{
			$this->dataParams=array_slice($this->dataParams,0,$param);
		}
		return $this;
	}
	function get(){
		return $this->dataParams;
	}

}

?>